Maura McCabe
mmccabe@uoregon.edu

This project takes an acp brevet time calculator and stores opening and closing times in a mongodb database.

How to Run:
from DockerRestApi run docker-compose up

From there localhost:5000 can be accessed to list:

    list all times
    list open times only
    list close times only

    list all times (csv format)
    list open times only (csv format)
    list close times only (csv format)

    list all times (json format)
    list open times only (json format)
    list close times only (json format)

    list all times (top 3 json)
    list all times (top 3 csv)
    list all times (top 5 json)
    list all times (top 5 csv)
    list all times (top 6 json)
    list all times (top 6 csv)

?top=<number> is a functionality added to any of the api resources. 
    with this only the top <number> of times are listed.



localhost:5001/listAll
localhost:5001/listAll/json
localhost:5001/listAll/csv
localhost:5001/listAll/json?top=3
localhost:5001/listAll/csv?top=3

localhost:5001/listOpenOnly
localhost:5001/listOpenOnly/json
localhost:5001/listOpenOnly/csv
localhost:5001/listOpenOnly/json?top=3
localhost:5001/listOpenOnly/csv?top=3

localhost:5001/listCloseOnly
localhost:5001/listCloseOnly/json
localhost:5001/listCloseOnly/csv
localhost:5001/listCloseOnly/json?top=3
localhost:5001/listCloseOnly/csv?top=3

These will display the database information for the specified type (list all, list open, etc) in dictionary 
(json) or csv (comma separated value) format. 





