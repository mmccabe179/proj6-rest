<html>
    <head>
        <title>Project 6 - Rest API</title>
    </head>

    <body>
        <h1>List All</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li> KM: $t->km  OPEN: $t->open  CLOSE: $t->close</li>";
            }
            ?>
        </ul>
        <h1>List Open</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listOpenOnly');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km OPEN: $t->open</li>";
                }
                ?>
            </ul>
            <h1>List Close</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listCloseOnly');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km CLOSE: $t->close</li>";
            }
            ?>
        </ul>
        <h1>List All JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li> KM: $t->km  OPEN: $t->open  CLOSE: $t->close</li>";
            }
            ?>
        </ul>
        <h1>List Open JSON</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listOpenOnly/json');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km OPEN: $t->open</li>";
                }
                ?>
            </ul>
            <h1>List Close JSON</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listCloseOnly/json');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km CLOSE: $t->close</li>";
            }
            ?>
        </ul>
        <h1>List All CSV</h1>
            <ul>
           <?php
            $contents = file_get_contents('http://laptop-service/listAll/csv');
            echo ($contents);
            ?>
        </ul>
         <h1>List Open CSV</h1>
            <ul>
           <?php
            $contents = file_get_contents('http://laptop-service/listOpenOnly/csv');
            echo ($contents);
            ?>
        </ul>
         <h1>List Close CSV</h1>
            <ul>
           <?php
            $contents = file_get_contents('http://laptop-service/listCloseOnly/csv');
            echo ($contents);
            ?>
        </ul>
        <h1>List All JSON top 3</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listAll/json?top=3');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km  OPEN: $t->open  CLOSE: $t->close</li>";
            }
            ?>
        </ul>
         <h1>List All CSV top 3</h1>
            <ul>
           <?php
            $contents = file_get_contents('http://laptop-service/listAll/csv?top=3');
            echo ($contents);
            ?>
        </ul>
        <h1>List All JSON top 5</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listAll/json?top=5');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km  OPEN: $t->open  CLOSE: $t->close</li>";
            }
            ?>
        </ul>
         <h1>List All CSV top 5</h1>
            <ul>
           <?php
            $contents = file_get_contents('http://laptop-service/listAll/csv?top=5');
            echo ($contents);
            ?>
        </ul>
        <h1>List All JSON top 6</h1>
            <ul>
                <?php
                $json = file_get_contents('http://laptop-service/listAll/json?top=6');
                $obj = json_decode($json);
                $times = $obj->result;
                foreach($times as $t) {
                    echo "<li> KM: $t->km  OPEN: $t->open  CLOSE: $t->close</li>";
            }
            ?>
        </ul>
         <h1>List All CSV top 6</h1>
            <ul>
           <?php
            $contents = file_get_contents('http://laptop-service/listAll/csv?top=6');
            echo ($contents);
            ?>
        </ul>



    </body>
</html>
