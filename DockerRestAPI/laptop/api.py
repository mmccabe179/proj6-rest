#! /usr/bin/env python
import json
import csv
import flask
from flask import Flask, request, Response
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
import os
import pymongo


# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient('172.18.0.2', 27017)
db = client.tododb


class listAll(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
        	result = getTimes(top, True, True)
        else:
        	result = getTimes(None, True, True)
        return flask.jsonify(result=result)

class listOpenOnly(Resource):
	def get(self):
		top = request.args.get('top', 0, type=int)
		if top is not 0:
			result = getTimes(top, True, False)
		else:
			result = getTimes(None, True, False)
		return flask.jsonify(result=result)

class listCloseOnly(Resource):
	def get(self):
		top = request.args.get('top', 0, type=int)
		if top is not 0:
			result = getTimes(top, False, True)
		else:
			result = getTimes(None, False, True)
		return flask.jsonify(result=result)

class listAllCSV(Resource):
	def get(self):
		top = request.args.get('top', 0, type=int)
		if top == None:
			top = 20
		_items = db.tododb.find().limit(int(top))
		items = [item for item in _items]
		csv = ""
		for item in items:
			csv += item['open'] + ', ' + item['close'] + ', '
		return csv

class listOpenOnlyCSV(Resource):
	def get(self):
		top = request.args.get('top', 0, type=int)
		if top == None:
			top = 20
		_items = db.tododb.find().limit(int(top))
		items = [item for item in _items]
		csv = ""
		for item in items:
			csv += item['open'] + ', '
		return csv

class listCloseOnlyCSV(Resource):
	def get(self):
		top = request.args.get('top', 0, type=int)
		if top == None:
			top = 20
		_items = db.tododb.find().limit(int(top))
		items = [item for item in _items]
		csv = ""
		for item in items:
			csv += item['close'] + ', '
		return csv


def getTimes(top,Open,Close):
	limit = 20

	if top is not None:
		limit = top

	times = db.tododb.find().limit(int(limit))
	result = []
	for item in times:
		if Open and Close:
			result.append({
				'open': item['open'],
				'close': item['close'],
				'km': item['km']
				})
		elif Open:
			result.append({
				'open': item['open'],
				'km': item['km']
				})
		else:
			result.append({
				'close': item['close'],
				'km': item['km']
				})
	return result


	
# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
